package campaignconfigurator.service.impl;

import campaignconfigurator.domain.Campaign;
import campaignconfigurator.domain.Segment;
import campaignconfigurator.repository.CampaignRepository;
import campaignconfigurator.service.ChildrenReferenceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.LinkedList;
import java.util.List;

@Slf4j
@Service
public class CampaignService{

    private final CampaignRepository campaignRepository;
    private final ChildrenReferenceService<Segment> segmentReferenceService;

    public CampaignService(CampaignRepository campaignRepository, ChildrenReferenceService<Segment> segmentReferenceService) {
        this.campaignRepository = campaignRepository;
        this.segmentReferenceService = segmentReferenceService;
    }

    public Mono<Campaign> getCampaign(String id){
        var campaignMono = campaignRepository.findById(id);
        return Mono
            .zip(
                campaignMono,
                populateCampaign(campaignMono)
            ).map(t->{
                var campaign = t.getT1();
                var segments = t.getT2();
                if(segments!=null && !segments.isEmpty()){
                    campaign.setSegments(new LinkedList<>());
                    segments.forEach(s-> campaign.getSegments().add(s));
                }
                return campaign;
            });
    }

    private Mono<List<Segment>> populateCampaign(Mono<Campaign> monoCampaign){
        return monoCampaign.flatMap(campaign -> {
            if(campaign.getSegmentIds()!=null && !campaign.getSegmentIds().isEmpty()){
                return segmentReferenceService
                    .getChildren(campaign.getSegmentIds())
                    .collectList();
            }
            return Mono.just(List.of());
        });
    }

    public Mono<Campaign> createCampaign(Campaign campaign){
        return campaignRepository.save(campaign);
    }

}
