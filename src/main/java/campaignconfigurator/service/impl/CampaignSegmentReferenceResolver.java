package campaignconfigurator.service.impl;

import campaignconfigurator.domain.Campaign;
import campaignconfigurator.domain.Segment;
import campaignconfigurator.repository.CampaignRepository;
import campaignconfigurator.repository.SegmentRepository;
import campaignconfigurator.service.ChildrenReferenceService;
import campaignconfigurator.service.ParentReferenceService;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.LinkedList;
import java.util.List;

@Component
public class CampaignSegmentReferenceResolver implements ParentReferenceService<Campaign>, ChildrenReferenceService<Segment> {

    private final CampaignRepository campaignRepository;
    private final SegmentRepository segmentRepository;

    public CampaignSegmentReferenceResolver(final CampaignRepository campaignRepository,
                                            final SegmentRepository segmentRepository) {
        this.campaignRepository = campaignRepository;
        this.segmentRepository = segmentRepository;
    }

    @Override
    public Flux<Segment> getChildren(List<String> childrenIds) {
        return segmentRepository.findAllById(childrenIds);
    }

    @Override
    public Mono<Boolean> parentExists(String parentId) {
        return campaignRepository.existsById(parentId);
    }

    @Override
    public Mono<Campaign> addChildReferenceToParent(final String segmentId, final String campaignId) {
        return campaignRepository
            .findById(campaignId)
            .map(campaign -> addSegmentReference(campaign,segmentId))
            .flatMap(campaignRepository::save);
    }

    private Campaign addSegmentReference(Campaign campaign, String segmentId){
        if(campaign.getSegmentIds() == null){
            campaign.setSegmentIds(new LinkedList<>());
        }
        campaign.getSegmentIds().add(segmentId);
        return campaign;
    }
}
