package campaignconfigurator.service.impl;

import campaignconfigurator.domain.Campaign;
import campaignconfigurator.domain.Scheduling;
import campaignconfigurator.domain.Segment;
import campaignconfigurator.error.CampaignNotFoundException;
import campaignconfigurator.error.SegmentNotFoundException;
import campaignconfigurator.repository.SegmentRepository;
import campaignconfigurator.service.ParentReferenceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.LinkedList;
import java.util.List;

@Slf4j
@Service
public class SegmentService {

    private final SegmentRepository segmentRepository;
    private final ParentReferenceService<Campaign> campaignReferenceService;

    public SegmentService(final SegmentRepository segmentRepository,
                          final ParentReferenceService<Campaign> campaignReferenceService) {
        this.segmentRepository = segmentRepository;
        this.campaignReferenceService = campaignReferenceService;
    }

    public Mono<Segment> getSegment(String id){
        return segmentRepository.findById(id);
    }

    /**
     * When we create a segment we have to add the reference to the parent campaign, so
     *      - check if the campaign exists
     *          - if it exists:
     *              1- save the segment first so the id will be created
     *              2- add the segment id to the parent
     *          - else return campaign not found exception
     *
     * @param segment
     * @param campaignId
     * @return
     * @throws CampaignNotFoundException
     */
    public Mono<Segment> createSegment(Segment segment, String campaignId){
        return campaignReferenceService
            .parentExists(campaignId)
            .flatMap(campaignExists -> {
                    if(campaignExists){
                        return segmentRepository
                            .save(segment)
                            .flatMap(segment1 -> addSegmentToCampaign(segment1, campaignId));
                    } else {
                        return Mono.error(new CampaignNotFoundException(campaignId));
                    }
                }
            );
    }

    private Mono<Segment> addSegmentToCampaign(Segment segment, String campaignId){
        campaignReferenceService
            .addChildReferenceToParent(segment.getId(), campaignId)
            .subscribe();
        return Mono.just(segment);
    }

    /**
     * To create a Scheduling we have to first find the parent Segment.
     * If the Segment exists we add the Scheduling to the Schedulings list and save the Segment,
     * otherwise we throw a SegmentNotFoundException
     * @param scheduling
     * @param segmentId
     * @return
     */
    public Mono<Segment> createAndAddScheduling(Scheduling scheduling, String segmentId){
        return segmentRepository
            .findById(segmentId)
            .flatMap(segment -> {
                if(segment == null){
                    return Mono.error(new SegmentNotFoundException(segmentId));
                } else {
                    if(segment.getSchedulings() == null || segment.getSchedulings().isEmpty()){
                        segment.setSchedulings(new LinkedList<>());
                    }
                    segment.getSchedulings().add(scheduling);
                    return segmentRepository.save(segment);
                }
            });
    }

}
