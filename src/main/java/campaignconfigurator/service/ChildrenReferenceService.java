package campaignconfigurator.service;

import reactor.core.publisher.Flux;

import java.util.List;

public interface ChildrenReferenceService<C> {
    Flux<C> getChildren(List<String> childrenIds);
}
