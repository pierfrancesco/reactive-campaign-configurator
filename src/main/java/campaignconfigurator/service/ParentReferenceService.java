package campaignconfigurator.service;

import campaignconfigurator.domain.Campaign;
import campaignconfigurator.domain.Segment;
import reactor.core.publisher.Mono;

/**
 * This interface is used to manage Parent Child relationship between entities
 * @param <P> Parent entiy
 */
public interface ParentReferenceService<P> {

    /**
     * Service that manage the Child entity can use this method to check if a parent entity with that id exists
     * @param parentId
     * @return
     */
    Mono<Boolean> parentExists(String parentId);

    /**
     * Service that manage the Child entity can use this method to add a reference to the Child into the Parent entity
     * @param childId
     * @param parentId
     * @return
     */
    Mono<P> addChildReferenceToParent(final String childId, final String parentId);

}
