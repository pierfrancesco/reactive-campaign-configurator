package campaignconfigurator.repository;

import campaignconfigurator.domain.Segment;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SegmentRepository extends ReactiveMongoRepository<Segment, String> {
}
