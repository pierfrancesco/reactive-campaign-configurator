package campaignconfigurator.error;

public class CampaignNotFoundException extends Exception {
    private final String campaignId;
    private static final String MESSAGE = "Could not find campaign with id [%s]";

    public CampaignNotFoundException(String campaignId) {
        this.campaignId = campaignId;
    }

    @Override
    public String getMessage(){
        return String.format(MESSAGE, campaignId);
    }

}
