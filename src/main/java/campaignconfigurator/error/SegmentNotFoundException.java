package campaignconfigurator.error;

public class SegmentNotFoundException extends Exception {
    private final String campaignId;
    private static final String MESSAGE = "Could not find segment with id [%s]";

    public SegmentNotFoundException(String campaignId) {
        this.campaignId = campaignId;
    }

    @Override
    public String getMessage(){
        return String.format(MESSAGE, campaignId);
    }
}
