package campaignconfigurator.config;

import lombok.experimental.UtilityClass;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

@UtilityClass
public class DateUtil {

    public Instant getNowInstant(){
        return Instant.now();
    }

    public ZonedDateTime instantToZonedDateTime(Instant instant){
        return ZonedDateTime.ofInstant(instant, ZoneOffset.UTC);
    }
}
