package campaignconfigurator.rest;

import campaignconfigurator.domain.Segment;
import campaignconfigurator.error.CampaignNotFoundException;
import campaignconfigurator.rest.transformer.SchedulingTransformer;
import campaignconfigurator.rest.transformer.SegmentTransformer;
import campaignconfigurator.rest.view.SegmentCreationForm;
import campaignconfigurator.rest.view.SegmentView;
import campaignconfigurator.rest.view.scheduling.SchedulingCreationForm;
import campaignconfigurator.service.impl.SegmentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/segments")
public class SegmentResource {

    private final SegmentService segmentService;

    private final SegmentTransformer segmentTransformer;

    private final SchedulingTransformer schedulingTransformer;

    public SegmentResource(final SegmentService segmentService,
                           final SegmentTransformer segmentTransformer,
                           final SchedulingTransformer schedulingTransformer) {
        this.segmentService = segmentService;
        this.segmentTransformer = segmentTransformer;
        this.schedulingTransformer = schedulingTransformer;
    }


    @GetMapping("{segmentId}")
    public Mono<ResponseEntity<SegmentView>> getSegment(@PathVariable("segmentId") String segmentId){
        return segmentService
            .getSegment(segmentId)
            .map(segment -> ResponseEntity.ok(segmentTransformer.documentToView(segment)))
            .defaultIfEmpty(ResponseEntity.of(Optional.empty()));
    }

    @PostMapping
    public Mono<ResponseEntity<SegmentView>> createSegment(@RequestBody @Valid SegmentCreationForm segmentCreationForm){
        var segmentDocument = segmentTransformer.createDocument(segmentCreationForm);
        return segmentService
            .createSegment(segmentDocument, segmentCreationForm.getCampaignId())
            .map(segment -> documentToResponseEntity(segment));
    }

    @PutMapping("{segmentId}/scheduling")
    public Mono<ResponseEntity<SegmentView>> addScheduling(@RequestBody @Valid SchedulingCreationForm segmentCreationForm,
                                                           @PathVariable("segmentId") String segmentId){
        var schedulingDocument = schedulingTransformer.createDocument(segmentCreationForm);
        return segmentService
            .createAndAddScheduling(schedulingDocument, segmentId)
            .map(segment -> documentToResponseEntity(segment));
    }

    private ResponseEntity<SegmentView> documentToResponseEntity(Segment segment){
        return new ResponseEntity<>(
            segmentTransformer.documentToView(segment),
            HttpStatus.CREATED
        );
    }

}
