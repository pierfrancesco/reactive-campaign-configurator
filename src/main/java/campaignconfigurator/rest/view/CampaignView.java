package campaignconfigurator.rest.view;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CampaignView {

    private String id;
    private ZonedDateTime createdDate;
    private String name;
    private String description;
    private List<SegmentView> segments;

}
