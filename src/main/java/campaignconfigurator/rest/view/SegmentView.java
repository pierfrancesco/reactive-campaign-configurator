package campaignconfigurator.rest.view;

import com.fasterxml.jackson.annotation.JsonInclude;
import campaignconfigurator.rest.view.scheduling.SchedulingView;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.List;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SegmentView {

    private String id;
    private ZonedDateTime createdDate;
    private String name;
    private String locale;
    private List<SchedulingView> schedulings;

}
