package campaignconfigurator.rest.view.products;

import campaignconfigurator.rest.ProductResource;
import lombok.experimental.UtilityClass;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Links;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

@UtilityClass
public class LinksCollection {
    public Link getProductById(String id){
        return WebMvcLinkBuilder.linkTo(
            WebMvcLinkBuilder.methodOn(ProductResource.class).getProductById(id)
        ).withRel("get-product");
    }

    public Links getProductsTypes(){
        return Links.of(
            WebMvcLinkBuilder.linkTo(
                WebMvcLinkBuilder.methodOn(ProductResource.class).getCategoryType()
            ).withRel("get-product-categories"),
            WebMvcLinkBuilder.linkTo(
                WebMvcLinkBuilder.methodOn(ProductResource.class).getProductType()
            ).withRel("get-product-types"),
            WebMvcLinkBuilder.linkTo(
                WebMvcLinkBuilder.methodOn(ProductResource.class).getPriceType()
            ).withRel("get-price-types"),
            WebMvcLinkBuilder.linkTo(
                WebMvcLinkBuilder.methodOn(ProductResource.class).createProduct(new ProductCreationForm())
            ).withRel("create-product")
        );

    }
}
