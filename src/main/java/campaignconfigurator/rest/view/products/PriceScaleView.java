package campaignconfigurator.rest.view.products;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PriceScaleView {

    private Boolean evenPriceScale;
    private List<ScaleView> scales;

}
