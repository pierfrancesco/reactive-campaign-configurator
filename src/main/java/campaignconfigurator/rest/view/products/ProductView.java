package campaignconfigurator.rest.view.products;

import com.fasterxml.jackson.annotation.JsonInclude;
import campaignconfigurator.domain.products.Category;
import campaignconfigurator.domain.products.ProductType;
import lombok.Builder;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

import java.time.ZonedDateTime;
import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductView extends RepresentationModel<ProductView> {

    private String id;
    private ZonedDateTime createdDate;
    private Category category;
    private ProductType productType;
    private String name;
    private String description;
    private List<PriceView> prices;

}
