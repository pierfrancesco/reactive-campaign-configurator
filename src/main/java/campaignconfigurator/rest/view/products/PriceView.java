package campaignconfigurator.rest.view.products;

import com.fasterxml.jackson.annotation.JsonInclude;
import campaignconfigurator.domain.products.PriceType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PriceView {

    private PriceType type;
    private Float amount;
    private PriceScaleView priceScale;

}
