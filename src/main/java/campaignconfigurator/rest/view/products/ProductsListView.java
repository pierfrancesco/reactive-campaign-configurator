package campaignconfigurator.rest.view.products;

import lombok.Builder;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class ProductsListView extends RepresentationModel<ProductsListView> {
    private List<ProductView> products = new ArrayList<>();
}
