package campaignconfigurator.rest.view.products;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScaleView {

    private Integer position;
    private Integer minutes;
    private Float multiplier;

}
