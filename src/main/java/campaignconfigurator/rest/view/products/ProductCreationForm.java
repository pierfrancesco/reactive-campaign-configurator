package campaignconfigurator.rest.view.products;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@Data
public class ProductCreationForm {

    @NotNull(message = "Category type cannot be null or invalid")
    private String category;
    @NotNull(message = "Product type cannot be null or invalid")
    private String productType;
    @NotNull(message = "Name cannot be null")
    private String name;
    @NotNull(message = "Description cannot be null")
    private String description;
    private List<PriceCreationForm> prices;

    @NoArgsConstructor
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PriceCreationForm{
        @NotNull(message = "Price Type cannot be null or invalid")
        private String type;
        @NotNull(message = "Price amount cannot be null")
        private Float amount;
        private PriceScaleCreationForm priceScale;
    }

    @NoArgsConstructor
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PriceScaleCreationForm{
        @NotNull(message = "PriceScale even cannot be null")
        private Boolean evenPriceScale;
        private List<ScaleCreationForm> scales;
    }

    @NoArgsConstructor
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ScaleCreationForm{
        @NotNull(message = "Scale position cannot be null")
        private Integer position;
        @NotNull(message = "Scale minutes cannot be null")
        private Integer minutes;
        @NotNull(message = "Scale multiplier cannot be null")
        private Float multiplier;
    }
}
