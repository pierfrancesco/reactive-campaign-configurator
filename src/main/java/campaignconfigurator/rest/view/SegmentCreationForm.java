package campaignconfigurator.rest.view;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Data
public class SegmentCreationForm {

    @NotNull
    private String name;
    @NotNull
    private String locale;
    @NotNull
    private String campaignId;

}
