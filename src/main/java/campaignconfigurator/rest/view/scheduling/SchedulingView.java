package campaignconfigurator.rest.view.scheduling;

import com.fasterxml.jackson.annotation.JsonInclude;
import campaignconfigurator.rest.view.products.ProductView;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SchedulingView {

    private ZonedDateTime createdDate;
    private String name;
    private String description;
    private List<ProductView> products;
    private DurationView duration;


}
