package campaignconfigurator.rest.view.scheduling;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class DaysOfTheWeekDurationView extends DurationView {

    private List<String> days;
    private Integer startHour;
    private Integer endHour;

}
