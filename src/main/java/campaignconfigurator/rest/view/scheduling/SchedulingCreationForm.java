package campaignconfigurator.rest.view.scheduling;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@Data
public class SchedulingCreationForm {
    @NotNull
    private String name;
    @NotNull
    private String description;
    private DurationCreationForm duration;

    @JsonDeserialize(as = DaysOfTheWeekDurationView.class)
    public static abstract class DurationCreationForm {
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DaysOfTheWeekDurationView extends DurationCreationForm {
        @NotNull
        private List<String> days;
        @NotNull
        private Integer startHour;
        @NotNull
        private Integer endHour;
    }
}
