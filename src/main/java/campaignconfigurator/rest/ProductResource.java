package campaignconfigurator.rest;

import campaignconfigurator.domain.products.Category;
import campaignconfigurator.domain.products.PriceType;
import campaignconfigurator.domain.products.ProductDocument;
import campaignconfigurator.domain.products.ProductType;
import campaignconfigurator.repository.ProductRepository;
import campaignconfigurator.rest.transformer.ProductTransformer;
import campaignconfigurator.rest.view.products.LinksCollection;
import campaignconfigurator.rest.view.products.ProductCreationForm;
import campaignconfigurator.rest.view.products.ProductView;
import campaignconfigurator.rest.view.products.ProductsListView;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/products")
public class ProductResource {

    private final ProductRepository productRepository;
    private final ProductTransformer productTransformer;

    public ProductResource(final ProductRepository productRepository,
                           final ProductTransformer productTransformer) {
        this.productRepository = productRepository;
        this.productTransformer = productTransformer;
    }

    @GetMapping
    public ResponseEntity<ProductsListView> getProducts(){
        var products = ProductsListView.builder().build();
        products.add(LinksCollection.getProductsTypes());
        return ResponseEntity.ok(products);
    }

    @GetMapping("category")
    public ResponseEntity<List<String>> getCategoryType(){
        return ResponseEntity.ok(
            Arrays.stream(Category.values())
            .map(value -> value.toString())
            .collect(Collectors.toList())
        );
    }

    @GetMapping("type")
    public ResponseEntity<List<String>> getProductType(){
        return ResponseEntity.ok(
            Arrays.stream(ProductType.values())
                .map(value -> value.toString())
                .collect(Collectors.toList())
        );
    }

    @GetMapping("prices/type")
    public ResponseEntity<List<String>> getPriceType(){
        return ResponseEntity.ok(
            Arrays.stream(PriceType.values())
                .map(value -> value.toString())
                .collect(Collectors.toList())
        );
    }

    @PostMapping
    public Mono<ResponseEntity<ProductView>> createProduct(@RequestBody @Valid ProductCreationForm productCreationForm){

        var product = productTransformer.createDocument(productCreationForm);
        return productRepository
            .save(product)
            .map(productDocument -> documentToResponseEntity(productDocument));

    }

    @GetMapping("{productId}")
    public Mono<ResponseEntity<ProductView>> getProductById(@PathVariable("productId") String productId){
        return productRepository
            .findById(productId)
            .map(productDocument -> ResponseEntity.ok(productTransformer.documentToView(productDocument)))
            .defaultIfEmpty(ResponseEntity.of(Optional.empty()))
            ;

    }

    private ResponseEntity<ProductView> documentToResponseEntity(ProductDocument productDocument){
        var productView = productTransformer.documentToView(productDocument);
        productView = productView.add(LinksCollection.getProductById(productView.getId()));
        return new ResponseEntity<>(productView, HttpStatus.CREATED);
    }

}
