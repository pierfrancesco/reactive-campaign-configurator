package campaignconfigurator.rest;

import campaignconfigurator.domain.Campaign;
import campaignconfigurator.rest.transformer.CampaignTransformer;
import campaignconfigurator.rest.view.CampaignCreationForm;
import campaignconfigurator.rest.view.CampaignView;
import campaignconfigurator.service.impl.CampaignService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/campaigns")
public class CampaignResource {

    private final CampaignService campaignService;

    private final CampaignTransformer campaignTransformer;


    public CampaignResource(final CampaignService campaignService,
                            final CampaignTransformer campaignTransformer) {
        this.campaignService = campaignService;
        this.campaignTransformer = campaignTransformer;
    }

    @GetMapping("{campaignId}")
    public Mono<ResponseEntity<CampaignView>> getCampaign(@PathVariable("campaignId") String campaignId){
        return campaignService
            .getCampaign(campaignId)
            .map(campaign -> ResponseEntity.ok(campaignTransformer.documentToView(campaign)))
            .defaultIfEmpty(ResponseEntity.of(Optional.empty()));
    }

    @PostMapping
    public Mono<ResponseEntity<CampaignView>> createCampaign(@RequestBody @Valid CampaignCreationForm campaignCreationForm){
        var campaignDocument = campaignTransformer.createDocument(campaignCreationForm);
        return campaignService
            .createCampaign(campaignDocument)
            .map(campaign -> documentToResponseEntity(campaign));
    }

    private ResponseEntity<CampaignView> documentToResponseEntity(Campaign campaign){
        return new ResponseEntity<>(
            campaignTransformer.documentToView(campaign),
            HttpStatus.CREATED
        );
    }

}
