package campaignconfigurator.rest;

import campaignconfigurator.error.CampaignNotFoundException;
import campaignconfigurator.error.SegmentNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ServerWebExchange;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;
import org.zalando.problem.StatusType;
import org.zalando.problem.spring.webflux.advice.security.SecurityAdviceTrait;
import reactor.core.publisher.Mono;

@org.springframework.web.bind.annotation.ControllerAdvice
public class ControllerAdvice implements SecurityAdviceTrait {

    @ExceptionHandler(CampaignNotFoundException.class)
    public Mono<ResponseEntity<Problem>> handleCampaignNotFoundException(CampaignNotFoundException ex, ServerWebExchange request){
        return create(Status.BAD_REQUEST, ex, request);
    }

    @ExceptionHandler(SegmentNotFoundException.class)
    public Mono<ResponseEntity<Problem>> handleSegmentNotFoundException(SegmentNotFoundException ex, ServerWebExchange request){
        return create(Status.BAD_REQUEST, ex, request);
    }
}
