package campaignconfigurator.rest.transformer;

import com.fasterxml.jackson.annotation.JsonInclude;
import campaignconfigurator.domain.products.*;
import campaignconfigurator.rest.view.products.*;
import campaignconfigurator.config.DateUtil;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@NoArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@Component
public class ProductTransformer {

    public ProductDocument createDocument(ProductCreationForm productCreationForm){
        var product = ProductDocument.builder()
            .createdDate(DateUtil.getNowInstant())
            .category(
                Category.valueOf(productCreationForm.getCategory())
            )
            .productType(
                ProductType.valueOf(productCreationForm.getProductType())
            )
            .name(productCreationForm.getName())
            .description(productCreationForm.getDescription());

        if(productCreationForm.getPrices() != null && !productCreationForm.getPrices().isEmpty()){
            var prices = new ArrayList<Price>();
            productCreationForm.getPrices()
                .forEach(
                    price -> prices.add(createPriceDocument(price))
                );
            product.prices(prices);
        }

        return product.build();

    }

    public ProductView documentToView(ProductDocument productDocument){

        var productView = ProductView.builder()
            .id(productDocument.getId())
            .createdDate(
                DateUtil.instantToZonedDateTime(productDocument.getCreatedDate())
            )
            .category(productDocument.getCategory())
            .productType(productDocument.getProductType())
            .name(productDocument.getName())
            .description(productDocument.getDescription())
            .build();

        if(productDocument.getPrices() != null && !productDocument.getPrices().isEmpty()){
            var prices = new ArrayList<PriceView>();
            productDocument.getPrices()
                .forEach(
                    price -> prices.add(priceDocumentToView(price))
                );
            productView.setPrices(prices);
        }

        return productView;
    }

    private PriceView priceDocumentToView(Price price){
        var priceView = PriceView.builder()
            .type(price.getType())
            .amount(price.getAmount())
            .build();

        if(price.getPriceScale() != null){
            priceView.setPriceScale(priceScaleDocumentToView(price.getPriceScale()));
        }

        return priceView;

    }

    private Price createPriceDocument(ProductCreationForm.PriceCreationForm priceCreationForm){
        var price = Price.builder()
            .type(PriceType.valueOf(priceCreationForm.getType()))
            .amount(priceCreationForm.getAmount());

        if(priceCreationForm.getPriceScale() != null){
            price.priceScale(createPriceScale(priceCreationForm.getPriceScale()));
        }

        return price.build();
    }

    private PriceScaleView priceScaleDocumentToView(PriceScale priceScale){

        var priceScaleView = PriceScaleView.builder()
            .evenPriceScale(priceScale.getEvenPriceScale())
            .build();

        if(priceScale.getScaleList() != null && !priceScale.getScaleList().isEmpty()){
            var scales = new ArrayList<ScaleView>();
            priceScale.getScaleList()
                .forEach(
                    scale -> scales.add(scaleDocumentToView(scale))
                );
            priceScaleView.setScales(scales);
        }

        return priceScaleView;
    }

    private PriceScale createPriceScale(ProductCreationForm.PriceScaleCreationForm priceScaleCreationForm){
        var priceScale = PriceScale.builder()
            .evenPriceScale(priceScaleCreationForm.getEvenPriceScale());

        if(priceScaleCreationForm.getScales() != null && !priceScaleCreationForm.getScales().isEmpty()){
            var scales = new ArrayList<Scale>();
            priceScaleCreationForm.getScales()
                .forEach(
                    scale -> scales.add(createScaleDocument(scale))
                );
            priceScale.scaleList(scales);
        }
        return priceScale.build();
    }

    private ScaleView scaleDocumentToView(Scale scale){
        return ScaleView.builder()
            .position(scale.getPosition())
            .minutes(scale.getMinutes())
            .multiplier(scale.getMultiplier())
            .build();
    }

    private Scale createScaleDocument(ProductCreationForm.ScaleCreationForm scaleCreationForm){
        return Scale.builder()
            .position(scaleCreationForm.getPosition())
            .minutes(scaleCreationForm.getMinutes())
            .multiplier(scaleCreationForm.getMultiplier())
            .build();

    }

}
