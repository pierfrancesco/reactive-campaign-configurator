package campaignconfigurator.rest.transformer;

import com.fasterxml.jackson.annotation.JsonInclude;
import campaignconfigurator.rest.view.scheduling.DurationView;
import campaignconfigurator.rest.view.scheduling.SchedulingCreationForm;
import campaignconfigurator.domain.DaysOfTheWeekDuration;
import campaignconfigurator.domain.Duration;
import campaignconfigurator.domain.Scheduling;
import campaignconfigurator.rest.view.scheduling.DaysOfTheWeekDurationView;
import campaignconfigurator.rest.view.scheduling.SchedulingView;
import campaignconfigurator.config.DateUtil;
import campaignconfigurator.rest.view.products.ProductView;
import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Component
public class SchedulingTransformer {

    private final ProductTransformer productTransformer;

    public SchedulingTransformer(final ProductTransformer productTransformer){
        this.productTransformer = productTransformer;
    }

    public Scheduling createDocument(SchedulingCreationForm schedulingCreationForm){
        var scheduling = new Scheduling();
        scheduling.setName(schedulingCreationForm.getName());
        scheduling.setDescription(schedulingCreationForm.getDescription());
        scheduling.setDuration(
            createDurationDocument(schedulingCreationForm.getDuration())
        );
        return scheduling;
    }

    public SchedulingView documentToView(Scheduling scheduling){

        var schedulingView = SchedulingView.builder()
            .createdDate(DateUtil.instantToZonedDateTime(scheduling.getCreatedDate()))
            .name(scheduling.getName())
            .description(scheduling.getDescription());

        if(scheduling.getProducts() != null && !scheduling.getProducts().isEmpty()){
            List<ProductView> productViewList = new ArrayList<>();
            scheduling.getProducts().stream()
                .forEach(product ->
                    productViewList.add(productTransformer.documentToView(product))
                );
            schedulingView.products(productViewList);
        }



        if(scheduling.getDuration() != null){
            schedulingView.duration(durationDocumentToView(scheduling.getDuration()));
        }

        return schedulingView.build();

    }

    private DurationView durationDocumentToView(Duration duration){
        DurationView durationView = null;
        if(duration instanceof DaysOfTheWeekDuration){
            var daysOfTheWeekDuration = (DaysOfTheWeekDuration) duration;
            durationView = DaysOfTheWeekDurationView.builder()
                .startHour(daysOfTheWeekDuration.getStartHour())
                .endHour(daysOfTheWeekDuration.getEndHour())
                .days(daysOfTheWeekDuration.getDays())
                .build();
        }
        return durationView;
    }

    private Duration createDurationDocument(SchedulingCreationForm.DurationCreationForm durationCreationForm){
        Duration duration = null;
        if(durationCreationForm instanceof SchedulingCreationForm.DaysOfTheWeekDurationView){
            var durationForm = (SchedulingCreationForm.DaysOfTheWeekDurationView)durationCreationForm;
            validateDaysOfWeek(durationForm.getDays());

            duration = DaysOfTheWeekDuration.builder()
                .days(durationForm.getDays())
                .startHour(durationForm.getStartHour())
                .endHour(durationForm.getEndHour())
                .build();
        }
        return duration;
    }

    private void validateDaysOfWeek(List<String> days){
        days.forEach(
            day -> DayOfWeek.valueOf(day)
        );
    }

}
