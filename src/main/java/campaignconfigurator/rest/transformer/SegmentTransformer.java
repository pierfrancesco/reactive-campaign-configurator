package campaignconfigurator.rest.transformer;

import campaignconfigurator.domain.Segment;
import campaignconfigurator.rest.view.scheduling.SchedulingView;
import campaignconfigurator.rest.view.SegmentCreationForm;
import campaignconfigurator.rest.view.SegmentView;
import campaignconfigurator.config.DateUtil;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SegmentTransformer {

    private final SchedulingTransformer schedulingTransformer;

    public SegmentTransformer(final SchedulingTransformer schedulingTransformer) {
        this.schedulingTransformer = schedulingTransformer;
    }

    public SegmentView documentToView(Segment segment){
        var segmentView = SegmentView.builder()
            .id(segment.getId())
            .createdDate(DateUtil.instantToZonedDateTime(segment.getCreatedDate()))
            .name(segment.getName())
            .locale(segment.getLocale());

        if(segment.getSchedulings() != null && !segment.getSchedulings().isEmpty()){
            List<SchedulingView> schedulingViews = new ArrayList<>();
            segment.getSchedulings()
                .forEach(
                    scheduling -> schedulingViews.add(schedulingTransformer.documentToView(scheduling))
                );
            segmentView.schedulings(schedulingViews);
        }

        return segmentView.build();
    }

    public Segment createDocument(SegmentCreationForm segmentCreationForm){
        var segment = new Segment();
        segment.setName(segmentCreationForm.getName());
        segment.setLocale(segmentCreationForm.getLocale());
        return segment;
    }
}
