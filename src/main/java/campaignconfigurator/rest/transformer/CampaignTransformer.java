package campaignconfigurator.rest.transformer;

import campaignconfigurator.domain.Campaign;
import campaignconfigurator.rest.view.CampaignCreationForm;
import campaignconfigurator.rest.view.CampaignView;
import campaignconfigurator.rest.view.SegmentView;
import campaignconfigurator.config.DateUtil;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CampaignTransformer {

    private final SegmentTransformer segmentTransformer;

    public CampaignTransformer(final SegmentTransformer segmentTransformer) {
        this.segmentTransformer = segmentTransformer;
    }

    public CampaignView documentToView(Campaign campaign){
        var campaignView = CampaignView.builder()
            .id(campaign.getId())
            .createdDate(DateUtil.instantToZonedDateTime(campaign.getCreatedDate()))
            .name(campaign.getName())
            .description(campaign.getDescription());

        if(campaign.getSegments() != null && !campaign.getSegments().isEmpty()){
            List<SegmentView> segmentViews = new ArrayList<>();
            campaign.getSegments().forEach(
                segment ->
                    segmentViews.add(segmentTransformer.documentToView(segment))
            );
            campaignView.segments(segmentViews);
        }

        return campaignView.build();
    }

    public Campaign createDocument(CampaignCreationForm campaignCreationForm){
        var campaign = new Campaign();
        campaign.setName(campaignCreationForm.getName());
        campaign.setDescription(campaignCreationForm.getDescription());
        return campaign;
    }

}
