package campaignconfigurator.domain;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Data
@Builder
public class DaysOfTheWeekDuration extends Duration {

    @Field
    private List<String> days;

    @Field
    private Integer startHour;

    @Field
    private Integer endHour;

}
