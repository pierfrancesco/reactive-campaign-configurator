package campaignconfigurator.domain.products;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Data
@Builder
public class PriceScale {

    @Field
    private Boolean evenPriceScale;

    @Field
    private List<Scale> scaleList;

}
