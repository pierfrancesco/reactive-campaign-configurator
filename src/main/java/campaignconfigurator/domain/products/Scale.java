package campaignconfigurator.domain.products;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
public class Scale {

    @JsonIgnore
    @Field
    private Integer position;
    @Field
    private Integer minutes;
    @Field
    private Float multiplier;

}
