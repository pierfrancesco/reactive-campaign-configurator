package campaignconfigurator.domain.products;

public enum PriceType {
    DAILY,
    TIMED
}
