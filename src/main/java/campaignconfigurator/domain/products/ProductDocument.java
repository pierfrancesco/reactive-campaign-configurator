package campaignconfigurator.domain.products;

import campaignconfigurator.config.DateUtil;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.List;

@Data
@Builder
@Document(collection = "product")
public class ProductDocument {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field
    private Instant createdDate;

    @Field
    private Category category;

    @Field
    private ProductType productType;

    @Field
    private String name;

    @Field
    private String description;

    @Field
    private List<Price> prices;

    @PostConstruct
    public void init(){
        this.createdDate = DateUtil.getNowInstant();
    }


}
