package campaignconfigurator.domain.products;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
public class Price {

    @Field
    private PriceType type;

    @Field
    private Float amount;

    @JsonIgnore
    @Field
    private PriceScale priceScale;

}
