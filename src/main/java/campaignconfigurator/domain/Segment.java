package campaignconfigurator.domain;

import campaignconfigurator.config.DateUtil;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.Instant;
import java.util.List;

@Data
@Document(collection = "segment")
public class Segment {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field
    private Instant createdDate;

    @Field
    private String name;

    @Field
    private String locale;

    @Field
    private List<Scheduling> schedulings;

    public Segment(){
        this.createdDate = DateUtil.getNowInstant();
    }

}
