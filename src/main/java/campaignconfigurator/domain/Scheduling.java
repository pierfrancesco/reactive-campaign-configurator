package campaignconfigurator.domain;

import campaignconfigurator.config.DateUtil;
import campaignconfigurator.domain.products.ProductDocument;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.Instant;
import java.util.List;

@Data
public class Scheduling {

    private static final long serialVersionUID = 1L;

    @Field
    private Instant createdDate;

    @Field
    private String name;

    @Field
    private String description;

    @Field
    private List<ProductDocument> products;

    @Field
    private Duration duration;

    public Scheduling(){
        this.createdDate = DateUtil.getNowInstant();
    }

}
