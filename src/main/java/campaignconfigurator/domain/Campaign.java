package campaignconfigurator.domain;

import campaignconfigurator.config.DateUtil;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.Instant;
import java.util.List;

@Data
@Document(collection = "campaign")
public class
Campaign {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field
    private Instant createdDate;

    @Field
    private String name;

    @Field
    private String description;

    @Field
    private List<String> segmentIds;

    @Transient
    private transient List<Segment> segments;

    public Campaign(){
        this.createdDate = DateUtil.getNowInstant();
    }

}
