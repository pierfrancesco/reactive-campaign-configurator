package campaignconfigurator;

import campaignconfigurator.domain.Campaign;
import campaignconfigurator.domain.DaysOfTheWeekDuration;
import campaignconfigurator.domain.Scheduling;
import campaignconfigurator.domain.Segment;
import campaignconfigurator.rest.view.*;
import campaignconfigurator.domain.products.Category;
import campaignconfigurator.domain.products.ProductDocument;
import campaignconfigurator.domain.products.ProductType;
import campaignconfigurator.rest.view.scheduling.SchedulingCreationForm;
import lombok.experimental.UtilityClass;

import java.time.*;
import java.util.List;

@UtilityClass
public class TestDataUtil {

    public CampaignView getCampaignView() {
        return CampaignView.builder()
            .id("asdf")
            .name("Rocco")
            .build();
    }

    public SegmentView getSegmentView(){
        return SegmentView.builder()
            .id("asdf")
            .name("Rocco")
            .build();
    }

    public SegmentCreationForm getSegmentCreationForm(){
        var segmentCreationForm = new SegmentCreationForm();
        segmentCreationForm.setName("asdf");
        segmentCreationForm.setLocale("RoccoLand");
        segmentCreationForm.setCampaignId("RoccoCampaign");
        return segmentCreationForm;
    }

    public CampaignCreationForm getCampaignCreationForm(){
        var campaignCreationForm = new CampaignCreationForm();
        campaignCreationForm.setName("asdf");
        campaignCreationForm.setDescription("Rocco");
        return campaignCreationForm;
    }

    public Campaign getCampaignNoSegments(){
        var campaign = new Campaign();
        campaign.setId("asdf");
        campaign.setName("Rocco");
        campaign.setCreatedDate(Instant.now());
        campaign.setDescription("Cicciolina");
        return  campaign;
    }

    public Campaign getCampaignWithSegmentIds(){
        var campaign = getCampaignNoSegments();
        campaign.setSegmentIds(
            List.of("asdf")
        );
        return campaign;
    }

    public Campaign getCampaignWithSegments(){
        var campaign = getCampaignNoSegments();
        campaign.setSegments(
            List.of(getSegmentNoScheduling())
        );
        return campaign;
    }

    public Segment getSegmentNoScheduling(){
        var segment = new Segment();
        segment.setId("asdf");
        segment.setLocale("RoccoLand");
        return segment;
    }

    public Segment getSegmentWithScheduling(){
        var segment = getSegmentNoScheduling();
        segment.setSchedulings(
            List.of(TestDataUtil.getSchedulingNoProductNoDuration())
        );
        return segment;
    }

    public ProductDocument getProductDocumentNoPrice() {
        return ProductDocument.builder()
            .id("asdf")
            .createdDate(LocalDateTime.ofInstant(ZonedDateTime.now().toInstant(), ZoneOffset.UTC).toInstant(ZoneOffset.UTC))
            .category(Category.PERMIT)
            .productType(ProductType.PARKING)
            .name("Rocco")
            .description("Cicciolina")
            .build();
    }

    public Scheduling getSchedulingWithSingleProduct(){
        var duration = DaysOfTheWeekDuration.builder()
            .days(List.of("Monday"))
            .startHour(8)
            .endHour(20)
            .build();
        var scheduling = new Scheduling();
        scheduling.setCreatedDate(Instant.now());
        scheduling.setName("Rocco");
        scheduling.setDescription("69");
        scheduling.setProducts(
            List.of(TestDataUtil.getProductDocumentNoPrice())
        );
        scheduling.setDuration(duration);
        return scheduling;
    }

    public Scheduling getSchedulingNoProductNoDuration(){
        var scheduling = new Scheduling();
        scheduling.setCreatedDate(Instant.now());
        scheduling.setName("Rocco");
        scheduling.setDescription("69");;
        return scheduling;
    }

    public SchedulingCreationForm getSchedulingCreationForm(){
        SchedulingCreationForm schedulingCreationForm = new SchedulingCreationForm();
        schedulingCreationForm.setName("Rocco");
        schedulingCreationForm.setDescription("69");
        schedulingCreationForm.setDuration(
            SchedulingCreationForm.DaysOfTheWeekDurationView.builder()
                .days(List.of(DayOfWeek.MONDAY.name()))
                .startHour(8)
                .endHour(20)
                .build()
        );
        return schedulingCreationForm;
    }

}
