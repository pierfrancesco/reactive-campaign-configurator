package campaignconfigurator.rest;

import campaignconfigurator.domain.products.Category;
import campaignconfigurator.domain.products.PriceType;
import campaignconfigurator.domain.products.ProductDocument;
import campaignconfigurator.repository.ProductRepository;
import campaignconfigurator.rest.transformer.ProductTransformer;
import campaignconfigurator.rest.view.products.ProductCreationForm;
import campaignconfigurator.rest.view.products.ProductView;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import java.util.List;

import static campaignconfigurator.rest.ProductResourceTest.TEST_USER_LOGIN;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

@WithMockUser(value = TEST_USER_LOGIN)
@ExtendWith(SpringExtension.class)
@WebFluxTest(ProductResource.class)
public class ProductResourceTest {

    static final String TEST_USER_LOGIN = "test";

    @MockBean
    private ProductRepository productRepositoryMock;
    @MockBean
    private ProductTransformer productTransformerMock;

    private WebTestClient mockMvc;

    @Autowired
    private ApplicationContext webApplicationContext;

    @BeforeEach()
    public void setup()
    {
        mockMvc =
            WebTestClient
                .bindToApplicationContext(webApplicationContext)
                .build()
                .mutateWith(csrf());
    }

    @Test
    public void should_get_product() throws Exception {
        var expectedProduct = ProductView.builder()
            .id("asdf")
            .name("Rocco")
            .description("Cicciolina")
            .build();
        var productDocument = ProductDocument.builder().build();
        Mockito.when(productRepositoryMock.findById(anyString()))
            .thenReturn(Mono.just(productDocument));
        Mockito.when(productTransformerMock.documentToView(any()))
            .thenReturn(expectedProduct);

        mockMvc
            .get()
            .uri("/products/123")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk()
            .expectBody()
            .jsonPath("id").isEqualTo(expectedProduct.getId())
            .jsonPath("name").isEqualTo(expectedProduct.getName())
            .jsonPath("description").isEqualTo(expectedProduct.getDescription());


        Mockito.verify(productRepositoryMock).findById("123");
        Mockito.verify(productTransformerMock).documentToView(productDocument);

    }

    @Test
    public void should_not_get_product() throws Exception {

        Mockito.when(productRepositoryMock.findById(anyString()))
            .thenReturn(Mono.empty());

        mockMvc.get()
            .uri("/products/123")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound();

        Mockito.verify(productRepositoryMock).findById("123");
        Mockito.verifyNoInteractions(productTransformerMock);

    }

    @Test
    public void should_create_product() throws Exception {

        var expectedScale1 = new ProductCreationForm.ScaleCreationForm();
        expectedScale1.setPosition(1);
        expectedScale1.setMinutes(2);
        expectedScale1.setMultiplier(0.5F);
        var expectedScale2 = new ProductCreationForm.ScaleCreationForm();
        expectedScale2.setMinutes(2);
        expectedScale2.setMultiplier(0.5F);
        var expectedPriceScale = new ProductCreationForm.PriceScaleCreationForm();
        expectedPriceScale.setEvenPriceScale(true);
        expectedPriceScale.setScales(
                List.of(expectedScale1,expectedScale2)
            );
        var expectedPrice1 = new ProductCreationForm.PriceCreationForm();
        expectedPrice1.setType(PriceType.TIMED.name());
        expectedPrice1.setAmount(2F);
        expectedPrice1.setPriceScale(expectedPriceScale);
        var expectedPrice2 = new ProductCreationForm.PriceCreationForm();
        expectedPrice2.setType(PriceType.DAILY.name());
        expectedPrice2.setAmount(5F);
        var expectedProduct = new ProductCreationForm();
        expectedProduct.setCategory(Category.PERMIT.name());
        expectedProduct.setProductType(PriceType.TIMED.name());
        expectedProduct.setName("Rocco");
        expectedProduct.setDescription("Cicciolina");
        expectedProduct.setPrices(
                List.of(expectedPrice1, expectedPrice2)
            );

        var product = ProductDocument.builder().build();
        var productView = ProductView.builder()
            .id("Rocco")
            .name("69")
            .build();
        Mockito.when(productTransformerMock.createDocument(any()))
            .thenReturn(product);
        Mockito.when(productRepositoryMock.save(any()))
            .thenReturn(Mono.just(product));
        Mockito.when(productTransformerMock.documentToView(any()))
            .thenReturn(productView);

        mockMvc.post()
            .uri("/products")
            .body(BodyInserters.fromValue(expectedProduct))
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isCreated()
            .expectBody()
            .jsonPath("id").isEqualTo(productView.getId())
            .jsonPath("name").isEqualTo(productView.getName());

        Mockito.verify(productRepositoryMock).save(product);
        Mockito.verify(productTransformerMock).createDocument(any());
        Mockito.verify(productTransformerMock).documentToView(product);

    }

}
