package campaignconfigurator.rest.transformer;

import campaignconfigurator.TestDataUtil;
import campaignconfigurator.rest.view.scheduling.SchedulingView;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import static org.mockito.Matchers.any;

public class SegmentTransformerTest {

    private SchedulingTransformer schedulingTransformerMock = Mockito.mock(SchedulingTransformer.class);

    private SegmentTransformer segmentTransformer = new SegmentTransformer(schedulingTransformerMock);

    @Test
    public void should_transform_document_to_view_with_scheduling(){
        var expectedSegment = TestDataUtil.getSegmentWithScheduling();
        var schedulingView = SchedulingView.builder().build();
        Mockito.when(schedulingTransformerMock.documentToView(any())).thenReturn(schedulingView);

        var actualSegment = segmentTransformer.documentToView(expectedSegment);

        Assertions.assertEquals(expectedSegment.getId(), actualSegment.getId());
        Assertions.assertEquals(ZonedDateTime.ofInstant(expectedSegment.getCreatedDate(), ZoneOffset.UTC), actualSegment.getCreatedDate());
        Assertions.assertEquals(expectedSegment.getName(), actualSegment.getName());
        Assertions.assertEquals(expectedSegment.getLocale(), actualSegment.getLocale());
        Assertions.assertEquals(schedulingView, actualSegment.getSchedulings().get(0));

        Mockito.verify(schedulingTransformerMock, Mockito.times(1)).documentToView(expectedSegment.getSchedulings().get(0));

    }

    @Test
    public void should_transform_document_to_view_when_null_scheduling(){
        var expectedSegment = TestDataUtil.getSegmentNoScheduling();

        var actualSegment = segmentTransformer.documentToView(expectedSegment);

        Assertions.assertEquals(expectedSegment.getId(), actualSegment.getId());
        Assertions.assertEquals(ZonedDateTime.ofInstant(expectedSegment.getCreatedDate(), ZoneOffset.UTC), actualSegment.getCreatedDate());
        Assertions.assertEquals(expectedSegment.getName(), actualSegment.getName());
        Assertions.assertEquals(expectedSegment.getLocale(), actualSegment.getLocale());
        Assertions.assertNull(actualSegment.getSchedulings());

        Mockito.verifyNoInteractions(schedulingTransformerMock);

    }

    @Test
    public void should_create_scheduling_document(){
        var segmentCreationForm = TestDataUtil.getSegmentCreationForm();

        var actualSegment = segmentTransformer.createDocument(segmentCreationForm);

        Assertions.assertNull(actualSegment.getId());
        Assertions.assertNotNull(actualSegment.getCreatedDate());
        Assertions.assertEquals(segmentCreationForm.getName(), actualSegment.getName());
        Assertions.assertEquals(segmentCreationForm.getLocale(), actualSegment.getLocale());
        Assertions.assertNull(actualSegment.getSchedulings());
    }

}
