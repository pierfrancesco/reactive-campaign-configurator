package campaignconfigurator.rest.transformer;

import campaignconfigurator.domain.products.*;
import campaignconfigurator.rest.view.products.ProductCreationForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

public class ProductTransformerTest {

    private ProductTransformer productTransformer = new ProductTransformer();

    @Test
    public void should_transform_document_to_view(){
        var expectedScale1 = Scale.builder()
            .position(1)
            .minutes(2)
            .multiplier(0.5F)
            .build();
        var expectedScale2 = Scale.builder()
            .minutes(2)
            .multiplier(0.5F)
            .build();
        var expectedPriceScale = PriceScale.builder()
            .evenPriceScale(true)
            .scaleList(
                List.of(expectedScale1,expectedScale2)
            )
            .build();
        var expectedPrice1 = Price.builder()
            .type(PriceType.TIMED)
            .amount(2F)
            .priceScale(expectedPriceScale)
            .build();
        var expectedPrice2 = Price.builder()
            .type(PriceType.DAILY)
            .amount(5F)
            .build();
        var expectedProduct = ProductDocument.builder()
            .id("asdf")
            .createdDate(LocalDateTime.ofInstant(ZonedDateTime.now().toInstant(), ZoneOffset.UTC).toInstant(ZoneOffset.UTC))
            .category(Category.PERMIT)
            .productType(ProductType.PARKING)
            .name("Rocco")
            .description("Cicciolina")
            .prices(
                List.of(expectedPrice1, expectedPrice2)
            )
            .build();

        var actualProduct = productTransformer.documentToView(expectedProduct);

        Assertions.assertEquals(expectedProduct.getId(), actualProduct.getId());
        Assertions.assertEquals(ZonedDateTime.ofInstant(expectedProduct.getCreatedDate(), ZoneOffset.UTC), actualProduct.getCreatedDate());
        Assertions.assertEquals(expectedProduct.getCategory(), actualProduct.getCategory());
        Assertions.assertEquals(expectedProduct.getProductType(), actualProduct.getProductType());
        Assertions.assertEquals(expectedProduct.getName(), actualProduct.getName());
        Assertions.assertEquals(expectedProduct.getDescription(), actualProduct.getDescription());

        var actualPrice1 = actualProduct.getPrices().get(0);
        Assertions.assertEquals(expectedPrice1.getType(), actualPrice1.getType());
        Assertions.assertEquals(expectedPrice1.getAmount(), actualPrice1.getAmount());

        var actualPrice2 = actualProduct.getPrices().get(1);
        Assertions.assertEquals(expectedPrice2.getType(), actualPrice2.getType());
        Assertions.assertEquals(expectedPrice2.getAmount(), actualPrice2.getAmount());
        Assertions.assertNull(actualPrice2.getPriceScale());

        var actualPriceScale = actualPrice1.getPriceScale();
        Assertions.assertEquals(expectedPriceScale.getEvenPriceScale(), actualPriceScale.getEvenPriceScale());

        var actualScale1 = actualPriceScale.getScales().get(0);
        Assertions.assertEquals(actualScale1.getPosition(), expectedScale1.getPosition());
        Assertions.assertEquals(actualScale1.getMinutes(), expectedScale1.getMinutes());
        Assertions.assertEquals(actualScale1.getMultiplier(), expectedScale1.getMultiplier());

        var actualScale2 = actualPriceScale.getScales().get(1);
        Assertions.assertNull(actualScale2.getPosition());
        Assertions.assertEquals(actualScale2.getMinutes(), expectedScale2.getMinutes());
        Assertions.assertEquals(actualScale2.getMultiplier(), expectedScale2.getMultiplier());

    }

    @Test
    public void should_create_new_document(){
        var expectedScale1 = new ProductCreationForm.ScaleCreationForm();
        expectedScale1.setPosition(1);
        expectedScale1.setMinutes(2);
        expectedScale1.setMultiplier(0.5F);
        var expectedScale2 = new ProductCreationForm.ScaleCreationForm();
        expectedScale2.setMinutes(2);
        expectedScale2.setMultiplier(0.5F);
        var expectedPriceScale = new ProductCreationForm.PriceScaleCreationForm();
        expectedPriceScale.setEvenPriceScale(true);
        expectedPriceScale.setScales(
            List.of(expectedScale1,expectedScale2)
        );
        var expectedPrice1 = new ProductCreationForm.PriceCreationForm();
        expectedPrice1.setType(PriceType.TIMED.name());
        expectedPrice1.setAmount(2F);
        expectedPrice1.setPriceScale(expectedPriceScale);
        var expectedPrice2 = new ProductCreationForm.PriceCreationForm();
        expectedPrice2.setType(PriceType.DAILY.name());
        expectedPrice2.setAmount(5F);
        var expectedProduct = new ProductCreationForm();
        expectedProduct.setCategory(Category.PERMIT.name());
        expectedProduct.setProductType(ProductType.PARKING.name());
        expectedProduct.setName("Rocco");
        expectedProduct.setDescription("Cicciolina");
        expectedProduct.setPrices(
            List.of(expectedPrice1, expectedPrice2)
        );

        var actualProduct = productTransformer.createDocument(expectedProduct);

        Assertions.assertNull(actualProduct.getId());
        Assertions.assertNotNull(actualProduct.getCreatedDate());
        Assertions.assertEquals(expectedProduct.getCategory(), actualProduct.getCategory().name());
        Assertions.assertEquals(expectedProduct.getProductType(), actualProduct.getProductType().name());
        Assertions.assertEquals(expectedProduct.getName(), actualProduct.getName());
        Assertions.assertEquals(expectedProduct.getDescription(), actualProduct.getDescription());

        var actualPrice1 = actualProduct.getPrices().get(0);
        Assertions.assertEquals(expectedPrice1.getType(), actualPrice1.getType().name());
        Assertions.assertEquals(expectedPrice1.getAmount(), actualPrice1.getAmount());

        var actualPrice2 = actualProduct.getPrices().get(1);
        Assertions.assertEquals(expectedPrice2.getType(), actualPrice2.getType().name());
        Assertions.assertEquals(expectedPrice2.getAmount(), actualPrice2.getAmount());
        Assertions.assertNull(actualPrice2.getPriceScale());

        var actualPriceScale = actualPrice1.getPriceScale();
        Assertions.assertEquals(expectedPriceScale.getEvenPriceScale(), actualPriceScale.getEvenPriceScale());

        var actualScale1 = actualPriceScale.getScaleList().get(0);
        Assertions.assertEquals(actualScale1.getPosition(), expectedScale1.getPosition());
        Assertions.assertEquals(actualScale1.getMinutes(), expectedScale1.getMinutes());
        Assertions.assertEquals(actualScale1.getMultiplier(), expectedScale1.getMultiplier());

        var actualScale2 = actualPriceScale.getScaleList().get(1);
        Assertions.assertNull(actualScale2.getPosition());
        Assertions.assertEquals(actualScale2.getMinutes(), expectedScale2.getMinutes());
        Assertions.assertEquals(actualScale2.getMultiplier(), expectedScale2.getMultiplier());

    }

    @Test
    public void should_not_create_new_document_when_invalid_product_category(){
        var expectedProduct = new ProductCreationForm();
        expectedProduct.setCategory("Rocco");
        expectedProduct.setProductType(ProductType.PARKING.name());
        expectedProduct.setName("Rocco");
        expectedProduct.setDescription("Cicciolina");

        Assertions.assertThrows(
            IllegalArgumentException.class,
            () -> productTransformer.createDocument(expectedProduct)
        );
    }

    @Test
    public void should_not_create_new_document_when_invalid_product_type(){
        var expectedProduct = new ProductCreationForm();
        expectedProduct.setCategory(Category.PERMIT.name());
        expectedProduct.setProductType("Rocco");
        expectedProduct.setName("Rocco");
        expectedProduct.setDescription("Cicciolina");

        Assertions.assertThrows(
            IllegalArgumentException.class,
            () -> productTransformer.createDocument(expectedProduct)
        );
    }

    @Test
    public void should_not_create_new_document_when_invalid_price_type(){
        var expectedPrice1 = new ProductCreationForm.PriceCreationForm();
        expectedPrice1.setType("Rocco");
        expectedPrice1.setAmount(2F);
        var expectedProduct = new ProductCreationForm();
        expectedProduct.setCategory(Category.PERMIT.name());
        expectedProduct.setProductType(ProductType.PARKING.name());
        expectedProduct.setName("Rocco");
        expectedProduct.setDescription("Cicciolina");
        expectedProduct.setPrices(
            List.of(expectedPrice1)
        );

        Assertions.assertThrows(
            IllegalArgumentException.class,
            () -> productTransformer.createDocument(expectedProduct)
        );
    }

}
