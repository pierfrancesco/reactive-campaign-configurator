package campaignconfigurator.rest.transformer;

import campaignconfigurator.TestDataUtil;
import campaignconfigurator.domain.DaysOfTheWeekDuration;
import campaignconfigurator.rest.view.scheduling.DaysOfTheWeekDurationView;
import campaignconfigurator.rest.view.scheduling.SchedulingCreationForm;
import campaignconfigurator.rest.view.products.ProductView;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import static org.mockito.Matchers.any;

public class SchedulingTransformerTest {

    private ProductTransformer productTransformerMock = Mockito.mock(ProductTransformer.class);


    private SchedulingTransformer schedulingTransformer = new SchedulingTransformer(productTransformerMock);

    @Test
    public void should_transform_document_to_view_with_products(){

        var expectedScheduling = TestDataUtil.getSchedulingWithSingleProduct();
        var productView = ProductView.builder().build();
        Mockito.when(productTransformerMock.documentToView(any())).thenReturn(productView);
        var actualScheduling = schedulingTransformer.documentToView(expectedScheduling);

        Assertions.assertEquals(ZonedDateTime.ofInstant(expectedScheduling.getCreatedDate(), ZoneOffset.UTC), actualScheduling.getCreatedDate());
        Assertions.assertEquals(expectedScheduling.getDescription(), actualScheduling.getDescription());
        Assertions.assertEquals(expectedScheduling.getName(), actualScheduling.getName());
        Assertions.assertEquals(List.of(productView), actualScheduling.getProducts());

        var expectedDuration = (DaysOfTheWeekDuration) expectedScheduling.getDuration();
        var actualeDuration = (DaysOfTheWeekDurationView) actualScheduling.getDuration();

        Assertions.assertEquals(expectedDuration.getDays(), actualeDuration.getDays());
        Assertions.assertEquals(expectedDuration.getStartHour(), actualeDuration.getStartHour());
        Assertions.assertEquals(expectedDuration.getEndHour(), actualeDuration.getEndHour());

        Mockito.verify(productTransformerMock, Mockito.times(1)).documentToView(expectedScheduling.getProducts().get(0));

    }

    @Test
    public void should_transform_document_to_view_when_null_duration_and_products(){

        var expectedScheduling = TestDataUtil.getSchedulingNoProductNoDuration();
        var actualScheduling = schedulingTransformer.documentToView(expectedScheduling);

        Assertions.assertEquals(ZonedDateTime.ofInstant(expectedScheduling.getCreatedDate(), ZoneOffset.UTC), actualScheduling.getCreatedDate());
        Assertions.assertEquals(expectedScheduling.getDescription(), actualScheduling.getDescription());
        Assertions.assertEquals(expectedScheduling.getName(), actualScheduling.getName());
        Assertions.assertEquals(expectedScheduling.getProducts(), actualScheduling.getProducts());
        Assertions.assertNull(expectedScheduling.getProducts());
        Assertions.assertNull(expectedScheduling.getDuration());

    }

    @Test
    public void should_create_scheduling_document(){
        var expectedScheduling = TestDataUtil.getSchedulingCreationForm();
        var actualScheduling = schedulingTransformer.createDocument(expectedScheduling);

        Assertions.assertNotNull(actualScheduling.getCreatedDate());
        Assertions.assertEquals(expectedScheduling.getDescription(), actualScheduling.getDescription());
        Assertions.assertEquals(expectedScheduling.getName(), actualScheduling.getName());
        Assertions.assertNull(actualScheduling.getProducts());
        var expectedSchedulingDuration = (SchedulingCreationForm.DaysOfTheWeekDurationView) expectedScheduling.getDuration();
        var actualSchedulingDuration = (DaysOfTheWeekDuration) actualScheduling.getDuration();
        Assertions.assertEquals(expectedSchedulingDuration.getDays(), actualSchedulingDuration.getDays());
        Assertions.assertEquals(expectedSchedulingDuration.getStartHour(), actualSchedulingDuration.getStartHour());
        Assertions.assertEquals(expectedSchedulingDuration.getEndHour(), actualSchedulingDuration.getEndHour());

    }

}
