package campaignconfigurator.rest.transformer;

import campaignconfigurator.TestDataUtil;
import campaignconfigurator.rest.view.SegmentView;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import static org.mockito.ArgumentMatchers.any;

public class CampaignTransformerTest {

    private SegmentTransformer segmentTransformerMock =
        Mockito.mock(SegmentTransformer.class);

    private  CampaignTransformer campaignTransformer =
        new CampaignTransformer(segmentTransformerMock);


    @Test
    public void should_transform_document_to_view_with_segments(){
        var expectedCampaign = TestDataUtil.getCampaignWithSegments();
        var segmentView = SegmentView.builder().build();
        Mockito.when(segmentTransformerMock.documentToView(any())).thenReturn(segmentView);
        var actualCampaign = campaignTransformer.documentToView(expectedCampaign);

        Assertions.assertEquals(expectedCampaign.getId(), actualCampaign.getId());
        Assertions.assertEquals(ZonedDateTime.ofInstant(expectedCampaign.getCreatedDate(), ZoneOffset.UTC), actualCampaign.getCreatedDate());
        Assertions.assertEquals(expectedCampaign.getName(), actualCampaign.getName());
        Assertions.assertEquals(expectedCampaign.getDescription(), actualCampaign.getDescription());
        Assertions.assertEquals(segmentView, actualCampaign.getSegments().get(0));

        Mockito.verify(segmentTransformerMock, Mockito.times(1)).documentToView(expectedCampaign.getSegments().get(0));

    }

    @Test
    public void should_transform_document_to_view_without_segments(){
        var expectedCampaign = TestDataUtil.getCampaignNoSegments();
        var actualCampaign = campaignTransformer.documentToView(expectedCampaign);

        Assertions.assertEquals(expectedCampaign.getId(), actualCampaign.getId());
        Assertions.assertEquals(ZonedDateTime.ofInstant(expectedCampaign.getCreatedDate(), ZoneOffset.UTC), actualCampaign.getCreatedDate());
        Assertions.assertEquals(expectedCampaign.getName(), actualCampaign.getName());
        Assertions.assertEquals(expectedCampaign.getDescription(), actualCampaign.getDescription());
        Assertions.assertNull(actualCampaign.getSegments());
    }

    @Test
    public void should_create_scheduling_document(){
        var expectedCampaign = TestDataUtil.getCampaignCreationForm();
        var actualCampaign = campaignTransformer.createDocument(expectedCampaign);

        Assertions.assertNull(actualCampaign.getId());
        Assertions.assertNotNull(actualCampaign.getCreatedDate());
        Assertions.assertEquals(expectedCampaign.getName(), actualCampaign.getName());
        Assertions.assertEquals(expectedCampaign.getDescription(), actualCampaign.getDescription());
        Assertions.assertNull(actualCampaign.getSegments());

    }

}
