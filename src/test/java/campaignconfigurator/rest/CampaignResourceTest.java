package campaignconfigurator.rest;

import campaignconfigurator.TestDataUtil;
import campaignconfigurator.domain.Campaign;
import campaignconfigurator.rest.transformer.CampaignTransformer;
import campaignconfigurator.service.impl.CampaignService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import static campaignconfigurator.rest.CampaignResourceTest.TEST_USER_LOGIN;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WithMockUser(value = TEST_USER_LOGIN)
@ExtendWith(SpringExtension.class)
@WebFluxTest(CampaignResource.class)
public class CampaignResourceTest {

    static final String TEST_USER_LOGIN = "test";

    @MockBean
    private CampaignService campaignServiceMock;

    @MockBean
    private CampaignTransformer campaignTransformerMock;

    private WebTestClient mockMvc;
    @Autowired
    private ApplicationContext webApplicationContext;

    @BeforeEach()
    public void setup()
    {
        //Init MockMvc Object and build
        mockMvc =
            WebTestClient
                .bindToApplicationContext(webApplicationContext)
                .build()
                .mutateWith(csrf());
    }

    @Test
    public void should_get_campaign() throws Exception {
        var expectedCampaign = TestDataUtil.getCampaignView();
        var campaignDoc = new Campaign();
        Mockito.when(campaignServiceMock.getCampaign(anyString())).thenReturn(Mono.just(campaignDoc));
        Mockito.when(campaignTransformerMock.documentToView(any())).thenReturn(expectedCampaign);

        mockMvc
            .get()
            .uri("/campaigns/123")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk()
            .expectBody()
            .jsonPath("id").isEqualTo(expectedCampaign.getId())
            .jsonPath("name").isEqualTo(expectedCampaign.getName());

        Mockito.verify(campaignServiceMock).getCampaign("123");
        Mockito.verify(campaignTransformerMock).documentToView(campaignDoc);
    }

    @Test
    public void should_not_get_campaign() throws Exception {
        Mockito.when(campaignServiceMock.getCampaign(anyString())).thenReturn(Mono.empty());

        mockMvc
            .get()
            .uri("/campaigns/123")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound();

        Mockito.verify(campaignServiceMock).getCampaign("123");
        Mockito.verifyNoInteractions(campaignTransformerMock);
    }

    @Test
    public void  should_create_campaign() throws Exception {
        var campaignCreationForm = TestDataUtil.getCampaignCreationForm();
        var expectedCampaign = TestDataUtil.getCampaignView();
        var campaign = new Campaign();
        Mockito.when(campaignTransformerMock.createDocument(any())).thenReturn(campaign);
        Mockito.when(campaignServiceMock.createCampaign(any())).thenReturn(Mono.just(campaign));
        Mockito.when(campaignTransformerMock.documentToView(any())).thenReturn(expectedCampaign);

        mockMvc
            .post()
            .uri("/campaigns")
            .body(BodyInserters.fromValue((campaignCreationForm)))
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isCreated()
            .expectBody()
            .jsonPath("id").isEqualTo(expectedCampaign.getId())
            .jsonPath("name").isEqualTo(expectedCampaign.getName());

        Mockito.verify(campaignTransformerMock).createDocument(campaignCreationForm);
        Mockito.verify(campaignServiceMock).createCampaign(campaign);
        Mockito.verify(campaignTransformerMock).documentToView(campaign);

    }

}
