package campaignconfigurator.rest;

import campaignconfigurator.TestDataUtil;
import campaignconfigurator.domain.Scheduling;
import campaignconfigurator.domain.Segment;
import campaignconfigurator.error.CampaignNotFoundException;
import campaignconfigurator.error.SegmentNotFoundException;
import campaignconfigurator.rest.transformer.SchedulingTransformer;
import campaignconfigurator.rest.transformer.SegmentTransformer;
import campaignconfigurator.rest.view.SegmentCreationForm;
import campaignconfigurator.rest.view.scheduling.SchedulingCreationForm;
import campaignconfigurator.service.impl.SegmentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import static campaignconfigurator.rest.SegmentResourceTest.TEST_USER_LOGIN;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

@WithMockUser(value = TEST_USER_LOGIN)
@ExtendWith(SpringExtension.class)
@WebFluxTest(SegmentResource.class)
public class SegmentResourceTest {

    static final String TEST_USER_LOGIN = "test";

    @MockBean
    private SegmentService segmentServiceMock;

    @MockBean
    private SegmentTransformer segmentTransformerMock;

    @MockBean
    private SchedulingTransformer schedulingTransformerMock;

    private WebTestClient mockMvc;
    @Autowired
    private ApplicationContext webApplicationContext;

    @BeforeEach()
    public void setup()
    {
        //Init MockMvc Object and build
        mockMvc =
            WebTestClient
                .bindToApplicationContext(webApplicationContext)
                .build()
                .mutateWith(csrf());
    }

    @Test
    public void should_get_segment() throws Exception {
        var expectedSegment = TestDataUtil.getSegmentView();
        var segmentDoc = new Segment();
        Mockito.when(segmentServiceMock.getSegment(anyString())).thenReturn(Mono.just(segmentDoc));
        Mockito.when(segmentTransformerMock.documentToView(any())).thenReturn(expectedSegment);

        mockMvc
            .get()
            .uri("/segments/123")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk()
            .expectBody()
            .jsonPath("id").isEqualTo(expectedSegment.getId())
            .jsonPath("name").isEqualTo(expectedSegment.getName());

        Mockito.verify(segmentServiceMock).getSegment("123");
        Mockito.verify(segmentTransformerMock).documentToView(segmentDoc);
    }

    @Test
    public void should_not_get_segment() throws Exception {
        Mockito.when(segmentServiceMock.getSegment(anyString())).thenReturn(Mono.empty());

        mockMvc
            .get()
            .uri("/segments/123")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound();

        Mockito.verify(segmentServiceMock).getSegment("123");
        Mockito.verifyNoInteractions(segmentTransformerMock);
    }

    @Test
    public void  should_create_segment() throws Exception {
        var segmentCreationForm = TestDataUtil.getSegmentCreationForm();
        var expectedSegment = TestDataUtil.getSegmentView();
        var segmentDoc = TestDataUtil.getSegmentNoScheduling();

        Mockito.when(segmentTransformerMock.createDocument(any()))
            .thenReturn(segmentDoc);
        Mockito.when(segmentServiceMock.createSegment(any(),anyString()))
            .thenReturn(Mono.just(segmentDoc));
        Mockito.when(segmentTransformerMock.documentToView(any()))
            .thenReturn(expectedSegment);

        mockMvc.post()
            .uri("/segments")
            .body(BodyInserters.fromValue(segmentCreationForm))
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isCreated()
            .expectBody()
            .jsonPath("id").isEqualTo(expectedSegment.getId())
            .jsonPath("name").isEqualTo(expectedSegment.getName());

        Mockito.verify(segmentTransformerMock).createDocument(segmentCreationForm);
        Mockito.verify(segmentServiceMock).createSegment(segmentDoc, segmentCreationForm.getCampaignId());
        Mockito.verify(segmentTransformerMock).documentToView(segmentDoc);

    }

    @Test
    public void  should_not_create_segment_when_name_null() throws Exception {
        var segmentCreationForm = new SegmentCreationForm();
        segmentCreationForm.setLocale("Rocco");

        mockMvc.post()
            .uri("/segments")
            .body(BodyInserters.fromValue(segmentCreationForm))
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isBadRequest();

        Mockito.verifyNoInteractions(segmentTransformerMock, segmentServiceMock);
    }

    @Test
    public void  should_not_create_segment_when_locale_null() throws Exception {
        var segmentCreationForm = new SegmentCreationForm();
        segmentCreationForm.setName("Rocco");

        mockMvc.post()
            .uri("/segments")
            .body(BodyInserters.fromValue(segmentCreationForm))
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isBadRequest();

        Mockito.verifyNoInteractions(segmentTransformerMock, segmentServiceMock);
    }

    @Test
    public void  should_not_create_segment_when_campaign_not_found() throws Exception {
        var segmentCreationForm = TestDataUtil.getSegmentCreationForm();
        var segmentDoc = TestDataUtil.getSegmentNoScheduling();

        Mockito.when(segmentTransformerMock.createDocument(any()))
            .thenReturn(segmentDoc);
        Mockito.when(segmentServiceMock.createSegment(any(),anyString()))
            .thenReturn(Mono.error(new CampaignNotFoundException("Rocco")));

        mockMvc.post()
            .uri("/segments")
            .body(BodyInserters.fromValue(segmentCreationForm))
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isBadRequest()
            .expectBody()
            .jsonPath("$.message").isEqualTo("Bad Request: Could not find campaign with id [Rocco]");

        Mockito.verify(segmentTransformerMock).createDocument(segmentCreationForm);
        Mockito.verify(segmentServiceMock).createSegment(segmentDoc, segmentCreationForm.getCampaignId());
        Mockito.verifyNoMoreInteractions(segmentTransformerMock);

    }

    @Test
    public void should_create_scheduling() throws Exception {

        var form = TestDataUtil.getSchedulingCreationForm();
        var scheduling = new Scheduling();
        var segment = new Segment();
        var expectedSegment = TestDataUtil.getSegmentView();

        Mockito.when(schedulingTransformerMock.createDocument(any()))
            .thenReturn(scheduling);
        Mockito.when(segmentServiceMock.createAndAddScheduling(any(), anyString()))
            .thenReturn(Mono.just(segment));
        Mockito.when(segmentTransformerMock.documentToView(any()))
            .thenReturn(expectedSegment);

        mockMvc.put()
            .uri("/segments/69/scheduling")
            .accept(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue(form))
            .exchange()
            .expectStatus().isCreated()
            .expectBody()
            .jsonPath("id").isEqualTo(expectedSegment.getId())
            .jsonPath("name").isEqualTo(expectedSegment.getName());

        Mockito.verify(schedulingTransformerMock).createDocument(form);
        Mockito.verify(segmentServiceMock).createAndAddScheduling(scheduling, "69");
        Mockito.verify(segmentTransformerMock).documentToView(segment);

    }

    @Test
    public void should_not_create_scheduling_when_segment_not_found() throws Exception {

        var form = TestDataUtil.getSchedulingCreationForm();
        var scheduling = new Scheduling();
        var segment = new Segment();

        Mockito.when(schedulingTransformerMock.createDocument(any()))
            .thenReturn(scheduling);
        Mockito.when(segmentServiceMock.createAndAddScheduling(any(), anyString()))
            .thenReturn(Mono.error(new SegmentNotFoundException("Rocco")));

        mockMvc.put()
            .uri("/segments/69/scheduling")
            .accept(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue(form))
            .exchange()
            .expectStatus().isBadRequest()
            .expectBody()
            .jsonPath("$.message").isEqualTo("Bad Request: Could not find segment with id [Rocco]");


        Mockito.verify(schedulingTransformerMock).createDocument(form);
        Mockito.verify(segmentServiceMock).createAndAddScheduling(scheduling, "69");
        Mockito.verifyNoMoreInteractions(segmentTransformerMock);

    }

    @Test
    public void should_not_create_scheduling_when_name_null() throws Exception {
        var creationForm = new SchedulingCreationForm();
        creationForm.setDescription("rocco");

        mockMvc
            .put()
            .uri("/segments/69/scheduling")
            .accept(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue(creationForm))
            .exchange()
            .expectStatus().isBadRequest();

        Mockito.verifyNoInteractions(schedulingTransformerMock, schedulingTransformerMock, segmentServiceMock);

    }

    @Test
    public void should_not_create_scheduling_when_description_null() throws Exception {
        var creationForm = new SchedulingCreationForm();
        creationForm.setName("rocco");

        mockMvc
            .put()
            .uri("/segments/69/scheduling")
            .accept(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue(creationForm))
            .exchange()
            .expectStatus().isBadRequest();

        Mockito.verifyNoInteractions(schedulingTransformerMock, schedulingTransformerMock, segmentServiceMock);

    }

}
