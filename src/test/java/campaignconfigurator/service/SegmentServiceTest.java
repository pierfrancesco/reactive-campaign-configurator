package campaignconfigurator.service;

import campaignconfigurator.TestDataUtil;
import campaignconfigurator.error.CampaignNotFoundException;
import campaignconfigurator.error.SegmentNotFoundException;
import campaignconfigurator.repository.SegmentRepository;
import campaignconfigurator.service.impl.SegmentService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


public class SegmentServiceTest {

    private final SegmentRepository segmentRepositoryMock =
        Mockito.mock(SegmentRepository.class);
    private final ParentReferenceService campaignReferenceServiceMock =
        Mockito.mock(ParentReferenceService.class);

    private SegmentService segmentService = new SegmentService(segmentRepositoryMock, campaignReferenceServiceMock);

    @Test
    public void should_get_segment_when_found(){
        var expectedSegment = TestDataUtil.getSegmentNoScheduling();
        String id = "Rocco";
        when(segmentRepositoryMock.findById(anyString()))
            .thenReturn(Mono.just(expectedSegment));

        StepVerifier
            .create(segmentService.getSegment(id))
            .assertNext(actualSegment ->{
                Assertions.assertEquals(expectedSegment.getId(), actualSegment.getId());
                Assertions.assertEquals(expectedSegment.getCreatedDate(), actualSegment.getCreatedDate());
                Assertions.assertEquals(expectedSegment.getName(), actualSegment.getName());
                Assertions.assertEquals(expectedSegment.getLocale(), actualSegment.getLocale());
                Assertions.assertNull(actualSegment.getSchedulings());
            })
            .verifyComplete();
        verify(segmentRepositoryMock).findById(id);
    }

    @Test
    public void should_get_null_when_segment_not_found(){
        String id = "Rocco";
        when(segmentRepositoryMock.findById(anyString()))
            .thenReturn(Mono.empty());

        StepVerifier
            .create(segmentService.getSegment(id))
            .expectComplete();
        verify(segmentRepositoryMock).findById(id);
    }

    @Test
    public void should_create_segment() {
        var expectedSegment = TestDataUtil.getSegmentNoScheduling();
        var expectedParentExists = Mono.just(true);
        var expectedCampaign = Mono.just(TestDataUtil.getCampaignNoSegments());
        String campaignId = "asdf";

        when(campaignReferenceServiceMock.parentExists(anyString())).thenReturn(expectedParentExists);
        when(segmentRepositoryMock.save(any())).thenReturn(Mono.just(expectedSegment));
        when(campaignReferenceServiceMock.addChildReferenceToParent(any(),anyString())).thenReturn(expectedCampaign);

        StepVerifier
            .create(segmentService.createSegment(expectedSegment, campaignId))
            .assertNext(actualSegment ->{
                Assertions.assertEquals(expectedSegment.getId(), actualSegment.getId());
                Assertions.assertEquals(expectedSegment.getCreatedDate(), actualSegment.getCreatedDate());
                Assertions.assertEquals(expectedSegment.getName(), actualSegment.getName());
                Assertions.assertEquals(expectedSegment.getLocale(), actualSegment.getLocale());
                Assertions.assertNull(actualSegment.getSchedulings());
            })
            .verifyComplete();
        verify(campaignReferenceServiceMock).parentExists(campaignId);
        verify(segmentRepositoryMock).save(expectedSegment);
        verify(campaignReferenceServiceMock).addChildReferenceToParent(expectedSegment.getId(), campaignId);
    }

    @Test
    public void should_not_create_segment_when_campaign_not_found(){
        var expectedSegment = TestDataUtil.getSegmentNoScheduling();
        var expectedParentNotExists = Mono.just(false);
        String campaignId = "asdf";
        when(campaignReferenceServiceMock.parentExists(anyString())).thenReturn(expectedParentNotExists);

        var error = segmentService.createSegment(expectedSegment, campaignId);

        StepVerifier
            .create(error)
            .expectError(CampaignNotFoundException.class);
        verify(campaignReferenceServiceMock).parentExists(campaignId);
        verifyNoMoreInteractions(campaignReferenceServiceMock);
        verifyNoInteractions(segmentRepositoryMock);
    }

    @Test
    public void should_create_scheduling() {
        var expectedSegment = TestDataUtil.getSegmentNoScheduling();
        var expectedScheduling = TestDataUtil.getSchedulingNoProductNoDuration();

        when(segmentRepositoryMock.findById(anyString())).thenReturn(Mono.just(expectedSegment));
        when(segmentRepositoryMock.save(any())).thenReturn(Mono.just(expectedSegment));

        var actualSegment = segmentService.createAndAddScheduling(expectedScheduling, expectedSegment.getId());

        StepVerifier
            .create(actualSegment)
            .assertNext(
                segment -> {
                    Assertions.assertEquals(expectedSegment.getId(), segment.getId());
                    Assertions.assertEquals(expectedSegment.getCreatedDate(), segment.getCreatedDate());
                    Assertions.assertEquals(expectedSegment.getName(), segment.getName());
                    Assertions.assertEquals(expectedSegment.getLocale(), segment.getLocale());
                    Assertions.assertEquals(1, segment.getSchedulings().size());
                    var scheduling = segment.getSchedulings().get(0);
                    Assertions.assertEquals(expectedScheduling.getCreatedDate(), scheduling.getCreatedDate());
                    Assertions.assertEquals(expectedScheduling.getDescription(), scheduling.getDescription());
                    Assertions.assertEquals(expectedScheduling.getName(), scheduling.getName());
                    Assertions.assertEquals(expectedScheduling.getProducts(), scheduling.getProducts());
                    Assertions.assertNull(expectedScheduling.getProducts());
                    Assertions.assertNull(expectedScheduling.getDuration());
                }
            )
            .verifyComplete();
        verify(segmentRepositoryMock).findById(expectedSegment.getId());
        verify(segmentRepositoryMock).save(expectedSegment);
    }

    @Test
    public void should_not_create_scheduling_when_segment_not_found(){
        var expectedScheduling = TestDataUtil.getSchedulingNoProductNoDuration();
        String segmentId = "asdf";
        when(segmentRepositoryMock.findById(anyString())).thenReturn(Mono.empty());

        var error = segmentService.createAndAddScheduling(expectedScheduling, segmentId);

        StepVerifier
            .create(error)
            .expectError(SegmentNotFoundException.class);
        verify(segmentRepositoryMock).findById(segmentId);
        verifyNoMoreInteractions(segmentRepositoryMock);
        verifyNoInteractions(campaignReferenceServiceMock);
    }


}
