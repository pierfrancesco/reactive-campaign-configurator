package campaignconfigurator.service;

import campaignconfigurator.TestDataUtil;
import campaignconfigurator.repository.CampaignRepository;
import campaignconfigurator.repository.SegmentRepository;
import campaignconfigurator.service.impl.CampaignSegmentReferenceResolver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class CampaignSegmentReferenceResolverTest {
    private CampaignRepository campaignRepositoryMock = Mockito.mock(CampaignRepository.class);
    private SegmentRepository segmentRepositoryMock = Mockito.mock(SegmentRepository.class);

    private CampaignSegmentReferenceResolver campaignSegmentReferenceResolver =
        new CampaignSegmentReferenceResolver(campaignRepositoryMock,segmentRepositoryMock);

    @Test
    public void should_get_children(){
        var expectedSegment = TestDataUtil.getSegmentNoScheduling();
        var segmentsFlux = List.of(expectedSegment.getId());
        when(segmentRepositoryMock.findAllById((Iterable<String>) any())).thenReturn(Flux.just(expectedSegment));


        StepVerifier
            .create(campaignSegmentReferenceResolver.getChildren(segmentsFlux))
            .assertNext(actualSegment ->{
                Assertions.assertEquals(expectedSegment.getId(), actualSegment.getId());
                Assertions.assertEquals(expectedSegment.getCreatedDate(), actualSegment.getCreatedDate());
                Assertions.assertEquals(expectedSegment.getName(), actualSegment.getName());
                Assertions.assertEquals(expectedSegment.getLocale(), actualSegment.getLocale());
                Assertions.assertNull(actualSegment.getSchedulings());
            })
            .verifyComplete();
        verify(segmentRepositoryMock).findAllById(segmentsFlux);
    }

    @Test
    public void should_be_true_when_campaign_exists(){
        String id = "Rocco";
        when(campaignRepositoryMock.existsById(anyString())).thenReturn(Mono.just(true));

        StepVerifier
            .create(campaignSegmentReferenceResolver.parentExists(id))
            .assertNext(campaignExists -> Assertions.assertTrue(campaignExists))
            .verifyComplete();
        verify(campaignRepositoryMock).existsById(id);

    }

    @Test
    public void should_be_false_when_campaign_not_exists(){
        String id = "Rocco";
        when(campaignRepositoryMock.existsById(anyString())).thenReturn(Mono.just(false));

        StepVerifier
            .create(campaignSegmentReferenceResolver.parentExists(id))
            .assertNext(campaignExists -> Assertions.assertFalse(campaignExists))
            .verifyComplete();
        verify(campaignRepositoryMock).existsById(id);
    }

    @Test
    public void should_add_segment_reference(){
        String expectedSegment = "Rocco";
        var expectedCampaign1 = Mono.just(TestDataUtil.getCampaignNoSegments());
        var expectedCampaign2 = Mono.just(TestDataUtil.getCampaignWithSegmentIds());

        when(campaignRepositoryMock.findById(anyString())).thenReturn(expectedCampaign1);
        when(campaignRepositoryMock.save(any())).thenReturn(expectedCampaign2);


        var actualCampaign = campaignSegmentReferenceResolver.addChildReferenceToParent(expectedSegment, "asdf");

        //This is expectedCampaign2 returned by the mock, not much to very but just that we return what the save gives us
        StepVerifier
            .create(actualCampaign)
            .expectNextMatches(campaign -> campaign.getSegmentIds() != null)
            .verifyComplete();

        //This is returned from the findById and the campaign has no segments
        //So this verifies that the segments are added by the method
        StepVerifier
            .create(expectedCampaign1)
            .assertNext(campaign ->{
                Assertions.assertTrue(campaign.getSegmentIds() != null);
                Assertions.assertEquals(1, campaign.getSegmentIds().size());
                var actualSegment = campaign.getSegmentIds().get(0);
                Assertions.assertEquals(expectedSegment, actualSegment);

                verify(campaignRepositoryMock).save(campaign);
            })
            .verifyComplete();

        verify(campaignRepositoryMock).findById("asdf");

    }

    @Test
    public void should_not_add_segment_reference_if_campaign_not_found(){
        var expectedSegment = "Rocco";
        var campaignId = "Rocco";

        when(campaignRepositoryMock.findById(anyString())).thenReturn(Mono.empty());

        campaignSegmentReferenceResolver.addChildReferenceToParent(expectedSegment, campaignId);

        verify(campaignRepositoryMock).findById(campaignId);
        verifyNoMoreInteractions(campaignRepositoryMock);
    }
}
