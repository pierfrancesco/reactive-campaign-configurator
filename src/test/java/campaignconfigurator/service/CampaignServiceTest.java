package campaignconfigurator.service;

import campaignconfigurator.TestDataUtil;
import campaignconfigurator.domain.Segment;
import campaignconfigurator.repository.CampaignRepository;
import campaignconfigurator.service.impl.CampaignService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class CampaignServiceTest {

    private CampaignRepository campaignRepositoryMock = Mockito.mock(CampaignRepository.class);
    private ChildrenReferenceService<Segment> segmentReferenceServiceMock = Mockito.mock(ChildrenReferenceService.class);

    private CampaignService campaignService = new CampaignService(campaignRepositoryMock, segmentReferenceServiceMock);

    @Test
    public void should_create_campaign(){
        var expectedCampaign = TestDataUtil.getCampaignNoSegments();
         when(campaignRepositoryMock.save(any())).thenReturn(Mono.just(expectedCampaign));

        StepVerifier
            .create(campaignService.createCampaign(expectedCampaign))
            .assertNext(actualCampaign ->{
                Assertions.assertEquals(expectedCampaign.getId(), actualCampaign.getId());
                Assertions.assertEquals(expectedCampaign.getCreatedDate(), actualCampaign.getCreatedDate());
                Assertions.assertEquals(expectedCampaign.getName(), actualCampaign.getName());
                Assertions.assertEquals(expectedCampaign.getDescription(), actualCampaign.getDescription());
                Assertions.assertNull(actualCampaign.getSegments());
            })
            .verifyComplete();
        verify(campaignRepositoryMock).save(expectedCampaign);

    }

    @Test
    public void should_get_campaing(){
        var expectedCampaign = TestDataUtil.getCampaignNoSegments();
        when(campaignRepositoryMock.findById(anyString())).thenReturn(Mono.just(expectedCampaign));

        StepVerifier
            .create(campaignService.getCampaign(expectedCampaign.getId()))
            .assertNext(actualCampaign ->{
                Assertions.assertEquals(expectedCampaign.getId(), actualCampaign.getId());
                Assertions.assertEquals(expectedCampaign.getCreatedDate(), actualCampaign.getCreatedDate());
                Assertions.assertEquals(expectedCampaign.getName(), actualCampaign.getName());
                Assertions.assertEquals(expectedCampaign.getDescription(), actualCampaign.getDescription());
                Assertions.assertNull(actualCampaign.getSegments());
            })
            .verifyComplete();
        verify(campaignRepositoryMock).findById(expectedCampaign.getId());
        verifyNoInteractions(segmentReferenceServiceMock);
    }

    @Test
    public void should_get_campaing_with_segments(){
        var expectedCampaign = TestDataUtil.getCampaignWithSegmentIds();
        var expectedSegment = TestDataUtil.getSegmentNoScheduling();
        var expectedSegmentsFlux = Flux.just(expectedSegment);
        when(campaignRepositoryMock.findById(anyString())).thenReturn(Mono.just(expectedCampaign));
        when(segmentReferenceServiceMock.getChildren(any())).thenReturn(expectedSegmentsFlux);

        StepVerifier
            .create(campaignService.getCampaign(expectedCampaign.getId()))
            .assertNext(actualCampaign ->{
                Assertions.assertEquals(expectedCampaign.getId(), actualCampaign.getId());
                Assertions.assertEquals(expectedCampaign.getCreatedDate(), actualCampaign.getCreatedDate());
                Assertions.assertEquals(expectedCampaign.getName(), actualCampaign.getName());
                Assertions.assertEquals(expectedCampaign.getDescription(), actualCampaign.getDescription());
                Assertions.assertEquals(1, actualCampaign.getSegments().size());
                var actualSegment = actualCampaign.getSegments().get(0);
                Assertions.assertEquals(expectedSegment.getId(), actualSegment.getId());
                Assertions.assertEquals(expectedSegment.getCreatedDate(), actualSegment.getCreatedDate());
                Assertions.assertEquals(expectedSegment.getName(), actualSegment.getName());
                Assertions.assertEquals(expectedSegment.getLocale(), actualSegment.getLocale());
                Assertions.assertNull(actualSegment.getSchedulings());
            })
            .verifyComplete();
        verify(campaignRepositoryMock).findById(expectedCampaign.getId());
        verify(segmentReferenceServiceMock).getChildren(any());

    }

    @Test
    public void should_get_when_not_found(){
        String id = "Rocco";
        when(campaignRepositoryMock.findById(anyString())).thenReturn(Mono.empty());

        StepVerifier
            .create(campaignService.getCampaign(id))
            .expectComplete();
        verify(campaignRepositoryMock).findById(id);
        verifyNoInteractions(segmentReferenceServiceMock);

    }



}
